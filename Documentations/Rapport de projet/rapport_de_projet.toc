\select@language {french}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Introduction}{4}
\contentsline {chapter}{\numberline {1}Rappels sur le cahier des charges}{5}
\contentsline {section}{\numberline {1.1}Choix du groupe}{5}
\contentsline {section}{\numberline {1.2}Membres du groupe}{6}
\contentsline {subsection}{\numberline {1.2.1}Nicolas \bsc {Acart} - \FB@og Jean Jacquelin \FB@fg \relax {}}{6}
\contentsline {subsection}{\numberline {1.2.2}Guillaume \bsc {Saladin} - \FB@og Guigui \FB@fg \relax {}}{7}
\contentsline {subsection}{\numberline {1.2.3}Antonin \bsc {Ginet} - \FB@og Ruybi \FB@fg \relax {}}{8}
\contentsline {section}{\numberline {1.3}Le projet}{9}
\contentsline {section}{\numberline {1.4}Partage des t\^aches}{11}
\contentsline {subsection}{\numberline {1.4.1}R\'epartition des t\^aches}{11}
\contentsline {subsection}{\numberline {1.4.2}Objectifs et d\'elais}{11}
\contentsline {chapter}{\numberline {2}Le projet}{12}
\contentsline {section}{\numberline {2.1}Visuel et Sonore}{12}
\contentsline {subsection}{\numberline {2.1.1}Th\`eme graphique}{12}
\contentsline {subsection}{\numberline {2.1.2}Sc\'enario}{13}
\contentsline {subsection}{\numberline {2.1.3}Graphismes}{13}
\contentsline {subsection}{\numberline {2.1.4}Son}{14}
\contentsline {subsection}{\numberline {2.1.5}Site Web}{14}
\contentsline {subsection}{\numberline {2.1.6}Level Design}{15}
\contentsline {section}{\numberline {2.2}Le jeu}{16}
\contentsline {subsection}{\numberline {2.2.1}Options}{16}
\contentsline {subsection}{\numberline {2.2.2}Personnage virtuel}{17}
\contentsline {subsection}{\numberline {2.2.3}Caract\'eristiques du joueur}{17}
\contentsline {subsection}{\numberline {2.2.4}Plateformes}{18}
\contentsline {subsection}{\numberline {2.2.5}Objets}{18}
\contentsline {subsection}{\numberline {2.2.6}Ennemis}{18}
\contentsline {section}{\numberline {2.3}Mode multijoueurs}{19}
\contentsline {subsection}{\numberline {2.3.1}Connexion}{19}
\contentsline {subsection}{\numberline {2.3.2}Gestion du r\'eseau}{20}
\contentsline {subsection}{\numberline {2.3.3}Syst\`eme de jeu}{20}
\contentsline {chapter}{\numberline {3}Avancement du projet}{21}
\contentsline {section}{\numberline {3.1}Premi\`ere soutenance}{21}
\contentsline {subsection}{\numberline {3.1.1}Narration et sc\'enario}{21}
\contentsline {subsection}{\numberline {3.1.2}D\'eveloppement des IA}{21}
\contentsline {subsection}{\numberline {3.1.3}Scripts joueurs}{22}
\contentsline {subsection}{\numberline {3.1.4}Level Design}{23}
\contentsline {subsection}{\numberline {3.1.5}Site Web}{23}
\contentsline {subsection}{\numberline {3.1.6}R\'esum\'e de l'avancement}{24}
\contentsline {section}{\numberline {3.2}Deuxi\`eme soutenance}{25}
\contentsline {subsection}{\numberline {3.2.1}D\'eveloppement des IA}{25}
\contentsline {subsection}{\numberline {3.2.2}Scripts joueurs}{26}
\contentsline {subsection}{\numberline {3.2.3}Level Design}{27}
\contentsline {subsection}{\numberline {3.2.4}Site Web}{28}
\contentsline {subsection}{\numberline {3.2.5}R\'esum\'e de l'avancement}{29}
\contentsline {section}{\numberline {3.3}Soutenance finale}{30}
\contentsline {subsection}{\numberline {3.3.1}R\'esum\'e du projet}{30}
\contentsline {chapter}{\numberline {4}Conclusions personnelles}{31}
\contentsline {section}{\numberline {4.1}Nicolas \bsc {Acart}}{31}
\contentsline {section}{\numberline {4.2}Guillaume \bsc {Saladin}}{32}
\contentsline {section}{\numberline {4.3}Antonin \bsc {Ginet}}{33}
\contentsline {chapter}{\relax \fontsize {14.4}{18}\selectfont \bf Conclusion}{34}
